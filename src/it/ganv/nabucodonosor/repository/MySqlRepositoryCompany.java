package it.ganv.nabucodonosor.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;


import it.ganv.nabucodonosor.Setting;
import it.ganv.nabucodonosor.domain.Company;
import it.ganv.nabucodonosor.domain.User;

public class MySqlRepositoryCompany {
	
	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void createSchema() {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER,PASSWORD )){
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA `nabucodonosor` ";
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}	
	}
	
	public static void createTableCompany() {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
		Statement statement = connection.createStatement();
		sql = "CREATE TABLE `nabucodonosor`.`company` (`idcompany` INT NOT NULL AUTO_INCREMENT, "
				+ "`businessname` VARCHAR(40) NOT NULL,"
				+"`vatnumber` VARCHAR(20) NOT NULL,"
				+"`fiscalnumber` VARCHAR(20) NOT NULL,"
				+"`registeredoffice` VARCHAR(100) NOT NULL,"
				+"`administrativeHeadquarters` VARCHAR(100) NOT NULL,"
				+"`pec` VARCHAR(100) NOT NULL,"
				+"`societytype` INT NOT NULL,"
				+"`socialcapital` FLOAT NOT NULL,"
				+"`idchieflegal` INT NOT NULL,"
				+"`note` VARCHAR(500) NOT NULL,"
				+"`website` VARCHAR(100) NOT NULL,"
				+"`deleted` TINYINT,"
				+"`attachments` VARCHAR(500) NOT NULL,"
				+" PRIMARY KEY (`idcompany`),"
				+ "FOREIGN KEY (`idchieflegal`) REFERENCES `user` (`iduser`))";
				
			statement.executeUpdate(sql);
			
			
		
		}catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	
	
}		public static void addCompany(Company company) {
	String sql;
	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
		Statement statement = connection.createStatement();
		sql = String.format(Locale.ENGLISH, "INSERT INTO company(businessname, vatnumber, fiscalnumber, registeredoffice,administrativeheadquarters,pec,societytype,socialcapital,idchieflegal,note,website,deleted, attachments) " + 
				"VALUES ('%s', '%s', '%s','%s','%s', '%s','%d',%f,'%d','%s','%s','%b','%s')",
				company.getBusinessName(),company.getVatNumber(),company.getFiscalNumber(),company.getRegisteredOffice(),company.getAdministrativeHeadquarters(),company.getPec(),
				company.getSocietyType(),company.getSocialCapital(),company.getIdChiefLegal(),company.getNote(),company.getWebSite(),company.getAttachments(),company.isDeleted());
		
		statement.executeUpdate(sql);
		
	} catch (SQLException e) {
		System.out.println("SQL exception");
		e.printStackTrace();
	}} //FINE ADD
	
	
	
public static void updateCompany(Company company,int idCompany) {
	String sql;

	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
		Statement statement = connection.createStatement();
		sql = String.format(Locale.ENGLISH, "UPDATE `nabucodonosor`.`company` SET `businessname` = '%s',"
				+ "`vatnumber` = '%s',"
				+ "`fiscalnumber` = '%s',"
				+ "`registeredoffice` = '%s',"
				+ "`administrativeheadquarters` = '%s',"
				+ "`pec` = '%s',"
				+ "`societytype` = %d,"
				+ "`socialcapital` = '%f',"
				+ "`idchieflegal` = %d,"
				+ "`note` = '%s',"
				+ "`website` = '%s',"
				+ "`deleted` = %b,"
				+ "`attachments` = '%s',"
				+ " WHERE `idCompany` = %d,",
				
				company.getBusinessName(),company.getVatNumber(),company.getFiscalNumber(),company.getRegisteredOffice(),company.getPec(),
				company.getSocietyType(),company.getSocialCapital(),company.getIdChiefLegal(),company.getNote(),company.getWebSite(),company.isDeleted(),
				company.getAttachments(),idCompany);

				statement.executeUpdate(sql);
				
				} catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}	
	


}}
