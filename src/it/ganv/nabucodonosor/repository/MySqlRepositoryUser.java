package it.ganv.nabucodonosor.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import it.ganv.nabucodonosor.Setting;
import it.ganv.nabucodonosor.domain.User;

public class MySqlRepositoryUser {
	
	
	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	
	public static void createSchemaUser() {
		String sql;
		try(Connection connection = DriverManager.getConnection(URL,USER,PASSWORD)){
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA`nabucodonosor`";
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	// fine create schemaaaaaaaaaaaa
	
	public static void createTableUser() {
	
		String sql;
		try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
			Statement statement = connection.createStatement();
			sql ="CREATE TABLE`nabucodonosor`.`user`(`iduser` INT NOT NULL AUTO_INCREMENT,"
					+ "`name` VARCHAR(30) NOT NULL,"
					+ "`surname` VARCHAR(30) NOT NULL,"
					+ "`fiscalcode` VARCHAR(30) NOT NULL,"
					+ "`vatnumber` VARCHAR(50) NOT NULL,"
					+ "`address` VARCHAR(100) NOT NULL,"
					+ "`email` VARCHAR(50) NOT NULL,"
					+ "`pec` VARCHAR(50) NOT NULL,"
					+ "`phonenumber` INT (30) NOT NULL,"
					+ "`cv` VARCHAR(50) NOT NULL,"
					+ "`birthdate` DATE NOT NULL,"
					+ "`note` VARCHAR (500) NOT NULL,"
					+ "`website` VARCHAR(100) NOT NULL,"
					+ "`deleted`TINYINT ,"
					+ "`attachments` VARCHAR(500) NOT NULL,"
					+ "PRIMARY KEY (`iduser`))";
			
			
			statement.executeUpdate(sql);
				
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	     }//fine create table
				
	
	public static void addUser(User user) {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = String.format(Locale.ENGLISH, "INSERT INTO user(name, surname, fiscalcode, vatnumber, address,email,pec,phonenumber, cv, birthdate, note, website,attachments, deleted) " + 
					"VALUES ('%s', '%s', '%s','%s','%s', '%s','%s',%d,'%s','%s','%s','%s','%s',%b)",
					user.getName(),user.getSurname(),user.getFiscalCode(),user.getVatNumber(),user.getEmail(),user.getPec(),
					user.getPhoneNumber(),user.getCv(),user.getBirthDate(),user.getNote(),user.getWebSite(),user.getAttachments(),user.isDeleted());
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}//FINE ADD
	
	
		
	}
	
	
	


