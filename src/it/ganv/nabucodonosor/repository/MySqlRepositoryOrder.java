package it.ganv.nabucodonosor.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import it.ganv.nabucodonosor.Setting;

public class MySqlRepositoryOrder {
	
	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void createSchemaOrder() {
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA `nabucodonosor` ";
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	public static void createTableOrder(){
		String sql;
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			sql = "CREATE TABLE `nabucodonosor`.`order`(`idorder`INT NOT NULL AUTO_INCREMENT,"
					+ "`idcustomer` INT NOT NULL,"
					+ "`price` FLOAT NOT NULL,"
					+ "`startdate` DATE NOT NULL,"
					+ "`stipulationdate` DATE NOT NULL,"
					+ "`deliverydate` DATE NOT NULL,"
					+ "`sal` INT NOT NULL,"
					+ "`salvalue` VARCHAR(50) NOT NULL,"
					+ "`idworkingteam` INT NOT NULL,"
					+ "`idrequestedservices` INT NOT NULL,"
					+ "`attachments` VARCHAR(500),"
					+ "`notes` VARCHAR(500),"
					+ "`delete` TINYINT,"
					+ "PRIMARY KEY (`idorder`),"
					+ "FOREIGN KEY (idworkingteam) REFERENCES workingteam(idworkingteam))";
				

			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
	
		}
	}
	
}
