package it.ganv.nabucodonosor.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import it.ganv.nabucodonosor.Setting;

public class MySqlRepositoryWorkingTeam {

	private static final String URL = Setting.URL;
	private static final String USER = Setting.USER;
	private static final String PASSWORD = Setting.PASSWORD;
	
	public static void createSchemaWorkingteam() {
		String sql;
		try(Connection connection = DriverManager.getConnection(URL,USER,PASSWORD)){
			Statement statement = connection.createStatement();
			sql = "CREATE SCHEMA`nabucodonosor`";
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	// fine create schemaaaaaaaaaaaa
	
	public static void createTableWorkingTeam() {
		
		String sql;
		try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){
			Statement statement = connection.createStatement();
			sql ="CREATE TABLE`nabucodonosor`.`workingteam`(`idworkingteam` INT NOT NULL AUTO_INCREMENT,"
					+ "`idcoordinator` INT NOT NULL,"
					+ "`numbercomponents` INT NOT NULL,"
					+ "`idusercomponents` VARCHAR(50) NULL,"
					+ "`note` VARCHAR (500) NOT NULL,"
					+ "`deleted`TINYINT ,"
					+ "`attachments` VARCHAR(500) NOT NULL,"
					+ "PRIMARY KEY (`idworkingteam`),"
					+ "FOREIGN KEY (`idcoordinator`) REFERENCES `user`(`iduser`))";
			
			System.out.println(sql);
			statement.executeUpdate(sql);
				
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
			}
	     }//fine create table
	
	
	
}
