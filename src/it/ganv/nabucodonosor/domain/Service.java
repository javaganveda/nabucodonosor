package it.ganv.nabucodonosor.domain;

public class Service {

	private int idService;
	private int tipology;
	private int price;
	private String description;
	private String attachments;
	private boolean delete;
	public int getIdService() {
		return idService;
	}
	public void setIdService(int idService) {
		this.idService = idService;
	}
	public int getTipology() {
		return tipology;
	}
	public void setTipology(int tipology) {
		this.tipology = tipology;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
