package it.ganv.nabucodonosor.domain;

public class Company {

	private int idCompany;
	private String businessName;  // ragione sociale 
	private String vatNumber;     // partita iva
	private String fiscalNumber;  //codice fiscale
	private String registeredOffice;  //sede legale
	private String administrativeHeadquarters;  // sede amministrativa
	private String pec; 
	private int societyType;  //tipo di societa
	private float socialCapital;  //capitale sociale
	private int idChiefLegal;  // responsabile legale
	private String note;      //note
	private String webSite;   //sito
	private boolean deleted;  // cancellare
	private String attachments;  //allegati

	
	
	
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public int getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getFiscalNumber() {
		return fiscalNumber;
	}
	public void setFiscalNumber(String fiscalNumber) {
		this.fiscalNumber = fiscalNumber;
	}
	public String getRegisteredOffice() {
		return registeredOffice;
	}
	public void setRegisteredOffice(String registeredOffice) {
		this.registeredOffice = registeredOffice;
	}
	public String getAdministrativeHeadquarters() {
		return administrativeHeadquarters;
	}
	public void setAdministrativeHeadquarters(String administrativeHeadquarters) {
		this.administrativeHeadquarters = administrativeHeadquarters;
	}
	public String getPec() {
		return pec;
	}
	public void setPec(String pec) {
		this.pec = pec;
	}
	public int getSocietyType() {
		return societyType;
	}
	public void setSocietyType(int societyType) {
		this.societyType = societyType;
	}
	public float getSocialCapital() {
		return socialCapital;
	}
	public void setSocialCapital(float socialCapital) {
		this.socialCapital = socialCapital;
	}
	public int getIdChiefLegal() {
		return idChiefLegal;
	}
	public void setIdChiefLegal(int idChiefLegal) {
		this.idChiefLegal = idChiefLegal;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
