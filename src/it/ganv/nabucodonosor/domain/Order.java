package it.ganv.nabucodonosor.domain;

import java.sql.Date;

public class Order {

	private int idOrder;
	private int idCustomer;
	private float price;
	private Date startDate;
	private Date stipulationDate;
	private Date deliveryDate;
	private int sal;
	private String salvalue;
	private int idWorkingteam;
	private int idRequestedServices;
	private String attachments;
	private String notes;
	private boolean delete;
	
	
	
	public int getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	public int getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getStipulationDate() {
		return stipulationDate;
	}
	public void setStipulationDate(Date stipulationDate) {
		this.stipulationDate = stipulationDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public int getSal() {
		return sal;
	}
	public void setSal(int sal) {
		this.sal = sal;
	}
	
	
	public String getSalvalue() {
		return salvalue;
	}
	
	public void setSalvalue(String salvalue) {
		this.salvalue = salvalue;
	}
	
	public int getIdWorkingteam() {
		return idWorkingteam;
	}
	public void setIdWorkingteam(int idWorkingteam) {
		this.idWorkingteam = idWorkingteam;
	}
	public int getIdRequestedServices() {
		return idRequestedServices;
	}
	public void setIdRequestedServices(int idRequestedServices) {
		this.idRequestedServices = idRequestedServices;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
