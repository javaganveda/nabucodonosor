package it.ganv.nabucodonosor.domain;

public class WorkingTeam {

	private int idWorkingTeam;
	private int numberComponents;
	private int idCoordinator;
	private String idUserComponents;
	private String attachments;
	private String notes;
	private boolean delete;
	
	public int getIdCoordinator() {
		return idCoordinator;
	}
	public void setIdCoordinator(int idCoordinator) {
		this.idCoordinator = idCoordinator;
	}
	public int getIdWorkingTeam() {
		return idWorkingTeam;
	}
	public void setIdWorkingTeam(int idWorkingTeam) {
		this.idWorkingTeam = idWorkingTeam;
	}
	public int getNumberComponents() {
		return numberComponents;
	}
	public void setNumberComponents(int numberComponents) {
		this.numberComponents = numberComponents;
	}
	public String getIdUserComponents() {
		return idUserComponents;
	}
	public void setIdUserComponents(String idUserComponents) {
		this.idUserComponents = idUserComponents;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	
}
